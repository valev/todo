package main

import (
	"time"
)

type Todo struct {
	id int

	title   string
	content string

	created time.Time
}

var todos map[int]*Todo
